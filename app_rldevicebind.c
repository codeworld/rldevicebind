
/*** MODULEINFO
	<support_level>core</support_level>
 ***/

#include "asterisk.h"

ASTERISK_FILE_VERSION(__FILE__, "$Revision$")

#include "asterisk/module.h"
#include "asterisk/channel.h"
#include "asterisk/pbx.h"
#include "asterisk/utils.h"
#include "asterisk/linkedlists.h"
#include "asterisk/devicestate.h"
#include "asterisk/cli.h"
#include "asterisk/astdb.h"
#include "asterisk/app.h"
#include "asterisk/stasis_channels.h"


static struct ao2_container *bind_channel_list;
AST_MUTEX_DEFINE_STATIC(unsubscribed_mutex);
static ast_cond_t unsubscribed_cond = PTHREAD_COND_INITIALIZER;
int subscribe_count = 0;
int module_unloading = 0;
static char * app = "RLDeviceBind";

struct bind_channel_item
{
    char channel_name[AST_CHANNEL_NAME];
    struct ao2_container * device_names;
    enum ast_channel_state channel_state;
    enum ast_device_state device_state;
    int hangupped;
    struct stasis_subscription * subscription_id;
};
struct device_state_pair
{
    char * device_name;
    enum ast_device_state device_state;
    AST_LIST_ENTRY(device_state_pair) node;
};
struct device_cli_item{
    char * device_name;
    char channels_string[250];
    struct ast_devstate_aggregate device_state;
};

AST_LIST_HEAD_NOLOCK(device_state_pair_list,device_state_pair);


static void destroy_bind_channel_item_cb(void *obj)
{
    struct bind_channel_item * item = obj;
    ast_debug(4,"RLDeviceBind for channel %s destroying \n",item->channel_name);
    ao2_ref(item->device_names,-1);
}
static void destroy_device_cli_item(void * obj){
    struct device_cli_item * item = obj;
    ao2_ref(item->device_name,-1);
}

static int device_cli_item_hash(const void * obj,const int flags){
    const struct device_cli_item* item = obj;
    const char * item_key = obj;
    int result;
    switch(flags & OBJ_SEARCH_MASK){
        case OBJ_SEARCH_OBJECT:
            item = obj;
            result = ast_str_hash(item->device_name);
            break;
        case OBJ_SEARCH_KEY:
            item_key = obj;
            result = ast_str_hash(item_key);
            break;
        default:
            ast_assert(0);
            return 0;

    }
    return result;
}
static int device_cli_item_sort(const void *l,const void *r,int flags){
    const struct device_cli_item *lobj = l;
    const struct device_cli_item *robj = r;
    const char * rkey;
    int result = 0;
    switch(flags & OBJ_SEARCH_MASK){
        case OBJ_SEARCH_OBJECT:
            robj = r;
            if(lobj == robj)result = 0;
            else result = strcmp(lobj->device_name,robj->device_name);
            break;
        case OBJ_SEARCH_KEY:
            rkey = r;
            result = strcmp(lobj->device_name,rkey);
            break;
        default:
            ast_assert(0);
    }
    return result;
}
static int device_cli_item_cmp(void* l,void* r,int flags){
    const struct device_cli_item *lobj = l;
    const struct device_cli_item *robj;
    const char * rkey;
    int cmp=0;
    switch(flags & OBJ_SEARCH_MASK){
        case OBJ_SEARCH_OBJECT:
            robj = r;
            if(lobj == robj)cmp = CMP_MATCH;
            else cmp = !strcmp(lobj->device_name,robj->device_name)?CMP_MATCH:0;
            break;
        case OBJ_SEARCH_KEY:
            rkey = r;
            cmp = !strcmp(lobj->device_name,rkey)?CMP_MATCH:0;
            break;
        default:
            robj = r;
            if(lobj == robj)cmp = CMP_MATCH;
    }
    return cmp;
}
static struct bind_channel_item *create_bind_channel_item(const char *channel_name, const char *device_name)
{
    struct bind_channel_item *new_item = NULL;
    if ((new_item = ao2_alloc_options(sizeof(*new_item), destroy_bind_channel_item_cb,AO2_ALLOC_OPT_LOCK_NOLOCK))) {
        ast_copy_string(new_item->channel_name, channel_name, sizeof(new_item->channel_name));
        new_item->device_names = ast_str_container_alloc_options(AO2_ALLOC_OPT_LOCK_NOLOCK,1);
        ast_str_container_add(new_item->device_names,device_name);
        new_item->hangupped = 0;
        ast_debug(4,"RL DeviceBind created for channel %s \n",new_item->channel_name);
    }
    return new_item;
}

static int bind_channel_hash_cb(const void *obj, const int flags)
{
    const struct bind_channel_item *item;
    const char * hash_key;
    switch(flags & OBJ_SEARCH_MASK){
        case OBJ_SEARCH_OBJECT:
            item = obj;
            hash_key = item->channel_name;
            break;
        default:
            ast_assert(0);
            return 0;

    }
    return ast_str_hash(hash_key);
}

static int bind_channel_cmp_cb(void *obj, void *arg, int flags)
{
    struct bind_channel_item *item_left = obj, *item_right;
    const char * key = arg;
    int cmp_result;
    switch(flags & OBJ_SEARCH_MASK){
        case OBJ_SEARCH_OBJECT:
            item_right = arg;
            cmp_result = !strcmp(item_left->channel_name,item_right->channel_name)?CMP_MATCH:0;
            break;
        case OBJ_SEARCH_KEY:
            cmp_result = !strcmp(item_left->channel_name,key)?CMP_MATCH:0;
            break;
        default:
            item_right = arg;
            if(item_left == item_right)cmp_result = CMP_MATCH;
    }

    return cmp_result;
}
static enum ast_device_state get_dev_state(const char * dev_name){
    //it is assumed that the bind_channel_list is already locked
    struct ast_devstate_aggregate state_agregate;
    struct ao2_iterator list_iter = ao2_iterator_init(bind_channel_list,AO2_ITERATOR_DONTLOCK);
    struct bind_channel_item * channel_item;
    enum ast_device_state result;
    int device_exists = 0;
    ast_devstate_aggregate_init(&state_agregate);
    while((channel_item = ao2_iterator_next(&list_iter))){
        char * found_dev_name = ao2_find(channel_item->device_names,dev_name,0);
        if(found_dev_name){
            device_exists = 1;
            ast_devstate_aggregate_add(&state_agregate,channel_item->device_state);
            ao2_ref(found_dev_name,-1);
        }
        ao2_ref(channel_item,-1);
    }
    ao2_iterator_destroy(&list_iter);
    if(!device_exists){
        result = AST_DEVICE_NOT_INUSE;
    }else{
        result = ast_devstate_aggregate_result(&state_agregate);
    }
    return result;
}
static void resend_new_state_by_item(struct bind_channel_item * channel_item){
    //Attention need lock container before use channel_item->device_names.
    //At this time exec_handler may add new device to channel_item
    ao2_lock(bind_channel_list);
    struct ao2_iterator dev_names_iter = ao2_iterator_init(channel_item->device_names,AO2_ITERATOR_DONTLOCK);
    char * cur_dev_name;
    struct device_state_pair_list state_list = AST_LIST_HEAD_NOLOCK_INIT_VALUE;
    while((cur_dev_name=ao2_iterator_next(&dev_names_iter))){
        enum ast_device_state dev_state = get_dev_state(cur_dev_name);
        struct device_state_pair* new_pair = ast_calloc(1,sizeof(struct device_state_pair));
        new_pair->device_state = dev_state;
        // not using unref on cur_dev_name is correct. It is used later.
        new_pair->device_name = cur_dev_name;
        AST_LIST_INSERT_TAIL(&state_list,new_pair,node);
    }
    ao2_iterator_destroy(&dev_names_iter);
    ao2_unlock(bind_channel_list);
    struct device_state_pair *cur_device_state;
    while((cur_device_state = AST_LIST_REMOVE_HEAD(&state_list,node))){
        ast_devstate_changed(cur_device_state->device_state,AST_DEVSTATE_CACHABLE,"RLDeviceBind:%s",cur_device_state->device_name);
        ast_debug(4,"RL DeviceBind %s set state to %s \n",cur_device_state->device_name,ast_devstate2str(cur_device_state->device_state));
        ao2_ref(cur_device_state->device_name,-1); //free iterator value
        ast_free(cur_device_state);
    }

}

static void handle_channel_message(void *userdata, struct stasis_subscription *sub, struct stasis_message *msg)
{
    struct bind_channel_item * cur_item = userdata;
    struct ast_channel_snapshot* channel_snapshot;
    const struct stasis_message_type * message_type = stasis_message_type(msg);
//    const char * type_name = stasis_message_type_name(message_type);
//    int is_final_message = stasis_subscription_final_message(sub,msg);


    if(stasis_subscription_final_message(sub,msg)){
        if(!cur_item->hangupped){
            cur_item->hangupped = 1;
            ao2_find(bind_channel_list,cur_item,OBJ_NODATA | OBJ_UNLINK);
            resend_new_state_by_item(cur_item);
        }

        ast_mutex_lock(&unsubscribed_mutex);
        subscribe_count--;
        if(subscribe_count==0 && module_unloading){
            ast_cond_signal(&unsubscribed_cond);
        }
        ast_mutex_unlock(&unsubscribed_mutex);
        ao2_ref(cur_item,-1);
        return;
    }
    if(message_type == ast_channel_snapshot_type()){
        channel_snapshot = stasis_message_data(msg);
        if(channel_snapshot){
            if(ast_test_flag(&channel_snapshot->flags,AST_FLAG_DEAD)){
                ao2_find(bind_channel_list,cur_item,OBJ_NODATA | OBJ_UNLINK);
                if(!cur_item->hangupped){
                    cur_item->hangupped = 1;
                    stasis_unsubscribe(sub);
                    resend_new_state_by_item(cur_item);
                }
            }else{
                if(cur_item->channel_state!=channel_snapshot->state){
                    cur_item->channel_state = channel_snapshot->state;
                    enum ast_device_state dev_state = ast_state_chan2dev(cur_item->channel_state);
                    if(dev_state!=cur_item->device_state){
                        cur_item->device_state = dev_state;
                        resend_new_state_by_item(cur_item);
                    }
                }
            }
        }
    }

}

static enum ast_device_state statebind_devstate_callback(const char *data)
{
    enum ast_device_state result;
    ao2_lock(bind_channel_list);
    result = get_dev_state(data);
    ao2_unlock(bind_channel_list);
    return result;
}


static int app_exec(struct ast_channel *chan, const char *data){
    char * parse;
    if(ast_strlen_zero(data)){
        ast_log(LOG_WARNING,"RLDeviceBind requires argument 'name'");
        return -1;
    }
    AST_DECLARE_APP_ARGS(args,
            AST_APP_ARG(name);
    );
    parse = ast_strdupa(data);
    AST_STANDARD_APP_ARGS(args,parse);

    ast_debug(1,"RLDeviceBind name:%s\n",args.name);
    const char * chan_name = ast_channel_name(chan);
    struct bind_channel_item * cur_item;
    ao2_lock(bind_channel_list);
    if((cur_item=ao2_find(bind_channel_list,chan_name,OBJ_SEARCH_KEY))){
        char * found = ao2_find(cur_item->device_names,args.name,OBJ_SEARCH_KEY);
        if(found){
            ao2_ref(found,-1);
        }else{
            ast_str_container_add(cur_item->device_names,args.name);
        }

    }else{
        cur_item = create_bind_channel_item(ast_channel_name(chan),args.name);
        ast_mutex_lock(&unsubscribed_mutex);
        //Added ref for subscribe arg
        ao2_ref(cur_item,1);
        cur_item->subscription_id = stasis_subscribe(ast_channel_topic(chan),handle_channel_message,cur_item);
        if(cur_item->subscription_id){
            subscribe_count++;
            ao2_link(bind_channel_list,cur_item);
        }
        ast_mutex_unlock(&unsubscribed_mutex);
        if(!cur_item->subscription_id){
            ast_log(LOG_ERROR,"Failed to subscribe to %s channel events",ast_channel_name(chan));
        }

    }
    ao2_unlock(bind_channel_list);
    if(!cur_item->subscription_id){
        ast_log(LOG_ERROR,"Failed to subscribe to %s channel events",ast_channel_name(chan));
    }else{
        ast_debug(4,"Binded channel %s with ringoline device %s\n",ast_channel_name(chan),args.name);
    }
    struct ast_channel_snapshot* chan_snapshot = ast_channel_snapshot_get_latest(ast_channel_uniqueid(chan));
    if(chan_snapshot){
        cur_item->channel_state = chan_snapshot->state;
        cur_item->device_state = ast_state_chan2dev(cur_item->channel_state);
        ao2_ref(chan_snapshot,-1);
    }
    resend_new_state_by_item(cur_item);
    ao2_ref(cur_item,-1);
    return 0;
}


static char *handle_cli_rldevice_list(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{

    switch (cmd) {
        case CLI_INIT:
            e->command = "rldevice list";
            e->usage =
                    "Usage: rldevice list\n"
                    "       List all bindings ringoline devices to channel via RLDeviceBind dialplan application \n";
            return NULL;
        case CLI_GENERATE:
            return NULL;
    }

    if (a->argc != e->args)
        return CLI_SHOWUSAGE;

    ast_cli(a->fd, "\n"
                   "---------------------------------------------------------------------\n"
                   "--- RL Device to channel bind --------------------------------------------\n"
                   "---------------------------------------------------------------------\n"
                   "\n");

    // turn inside out from {chan_name,device_names[]} to {device_name,channels_string}
    struct ao2_container * cli_device_list = ao2_container_alloc_hash(AO2_ALLOC_OPT_LOCK_NOLOCK,AO2_CONTAINER_ALLOC_OPT_DUPS_REJECT,1,device_cli_item_hash,device_cli_item_sort,device_cli_item_cmp);
    ao2_lock(bind_channel_list);
    struct bind_channel_item * cur_bind_item;
    struct device_cli_item * cur_cli_item;
    char * cur_device_name;
    struct ao2_iterator list_iter = ao2_iterator_init(bind_channel_list,0);
    while((cur_bind_item=ao2_iterator_next(&list_iter))){
        struct ao2_iterator device_names_iter = ao2_iterator_init(cur_bind_item->device_names,0);
        while((cur_device_name=ao2_iterator_next(&device_names_iter))){
            cur_cli_item = ao2_find(cli_device_list,cur_device_name,OBJ_SEARCH_KEY);
            if(!cur_cli_item){
                cur_cli_item = ao2_alloc(sizeof(*cur_cli_item),destroy_device_cli_item);
                cur_cli_item->channels_string[0] = 0;
                ao2_ref(cur_device_name,1);
                cur_cli_item->device_name = cur_device_name;
                ast_devstate_aggregate_init(&cur_cli_item->device_state);
                ao2_link(cli_device_list,cur_cli_item);
            }

            ast_devstate_aggregate_add(&cur_cli_item->device_state,cur_bind_item->device_state);
            size_t space_size = sizeof(cur_cli_item->channels_string)-strlen(cur_cli_item->channels_string)-1;
            if(!ast_strlen_zero(cur_cli_item->channels_string)){
                if(space_size){
                    strncat(cur_cli_item->channels_string,"&",space_size);
                    space_size--;
                }
            }
            if(space_size){
                strncat(cur_cli_item->channels_string,cur_bind_item->channel_name,space_size);
            }

            ao2_ref(cur_cli_item,-1);
            ao2_ref(cur_device_name,-1);
        }
        ao2_iterator_destroy(&device_names_iter);
        ao2_ref(cur_bind_item,-1);
    }
    ao2_iterator_destroy(&list_iter);
    ao2_unlock(bind_channel_list);

    struct ao2_iterator device_cli_iter = ao2_iterator_init(cli_device_list,0);
    while((cur_cli_item=ao2_iterator_next(&device_cli_iter))){
        ast_cli(a->fd,"Name:%s State:%s Channels:%s \n",cur_cli_item->device_name,ast_devstate_str(ast_devstate_aggregate_result(&cur_cli_item->device_state)),cur_cli_item->channels_string);
        ao2_ref(cur_cli_item,-1);
    }
    ao2_iterator_destroy(&device_cli_iter);
    ao2_ref(cli_device_list,-1);

    ast_cli(a->fd,
            "---------------------------------------------------------------------\n"
            "---------------------------------------------------------------------\n"
            "\n");

    return CLI_SUCCESS;
}


static struct ast_cli_entry cli_apprldevice[] = {
        AST_CLI_DEFINE(handle_cli_rldevice_list, "List currently known binding channel to ringoline devices"),
};



static int unload_module(void)
{
    int res = 0;

    res |= ast_unregister_application(app);
    res |= ast_cli_unregister_multiple(cli_apprldevice, ARRAY_LEN(cli_apprldevice));
    res |= ast_devstate_prov_del("RLDeviceBind");
    struct ao2_iterator list_iter = ao2_iterator_init(bind_channel_list,AO2_ITERATOR_UNLINK);
    struct bind_channel_item * channel_item;
    while((channel_item = ao2_iterator_next(&list_iter))){
        if(channel_item->subscription_id){
            stasis_unsubscribe(channel_item->subscription_id);
        }
        ao2_ref(channel_item,-1);
    }
    ao2_iterator_destroy(&list_iter);

    ast_mutex_lock(&unsubscribed_mutex);
    module_unloading = 1;
    if(subscribe_count){
        ast_cond_wait(&unsubscribed_cond,&unsubscribed_mutex);
    }
    ast_mutex_unlock(&unsubscribed_mutex);

    ao2_ref(bind_channel_list,-1);
    return res;
}

static int load_module(void)
{
    int res = 0;
    bind_channel_list = ao2_container_alloc(1, bind_channel_hash_cb, bind_channel_cmp_cb);
    if (!bind_channel_list) {
        return AST_MODULE_LOAD_DECLINE;
    }
    res |= ast_register_application_xml(app,app_exec);
    res |= ast_devstate_prov_add("RLDeviceBind", statebind_devstate_callback);
    res |= ast_cli_register_multiple(cli_apprldevice, ARRAY_LEN(cli_apprldevice));

    return res;
}

AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_LOAD_ORDER, "Gets or sets a ringoline device state in the dialplan",
    .support_level = AST_MODULE_SUPPORT_CORE,
    .load = load_module,
    .unload = unload_module,
    .load_pri = AST_MODPRI_DEVSTATE_PROVIDER,
);
